# SelfDrivingCar
Hi, thanks for checking out my repo.
This project is focused on building a self-driving car from scratch.  I am a high-school student who loves AI, python, electronics, and 3D printing.

Goals:
 - ✅Desing (CAD, electronics)
 - ✅Buy Hardware
 - ✅3D print
 - ✅Assemble
 - ✅Write hardware controllers
 - ✅Make the car remote controlled through PC first
 - ✅Find path and steer the car using thresholding
 - 🚧 [IN PROGGRESS] Create a Conv Neural network to control the car using camera input only. I am still not sure if this end-to-end approach is a going to work well, but going to try it anyway.
 
Please consider staring! This is an ongoing project I will make everything pretty later ;)

# Hardware Build:
![alt_text](https://github.com/Tomaslapes/SelfDrivingCar/blob/main/photos/IMG_20210422_230719_693.jpg?raw=true)
![alt_text](https://github.com/Tomaslapes/SelfDrivingCar/blob/main/photos/IMG_20210422_230719_726.jpg?raw=true)
![alt_text](https://github.com/Tomaslapes/SelfDrivingCar/blob/main/photos/IMG_20210422_230719_733.jpg?raw=true)
![alt_text](https://github.com/Tomaslapes/SelfDrivingCar/blob/main/photos/IMG_20210422_230719_755.jpg?raw=true)

# Driving using thresholding:
https://user-images.githubusercontent.com/51922469/130356273-998b995e-fd66-47f9-a51c-ea47b03e34a0.mp4

https://user-images.githubusercontent.com/51922469/130356721-b87510aa-22b6-452a-ae5a-6b9064f1aa80.mp4




